package com.bjpowernode.springboot.pojo;

public class City {
    private String name;
    private int age;
    private String stree;
    private String add;

    public City() {
    }

    public City(String name, int age, String stree) {
        this.name = name;
        this.age = age;
        this.stree = stree;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getStree() {
        return stree;
    }

    public void setStree(String stree) {
        this.stree = stree;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", stree='" + stree + '\'' +
                '}';
    }
}
